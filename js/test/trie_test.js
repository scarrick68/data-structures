// pt short for prefix tree so I can use the name trie elsewhere
// without confusion.
var pt = require('../trie')
var expect = require('chai').expect
var readline = require('readline')
var fs = require('fs')

describe('Tests Implementation', function(){
	describe('Insert an element into the trie', function(){
		it('Testing addWord method. Put elements into an empty trie.', function(){
			var trie = new pt.Trie();

			// regular strings
			expect(trie.addWord('a')).to.equal(true)
			expect(trie.addWord('ab')).to.equal(true)
			expect(trie.addWord('abs')).to.equal(true)
			expect(trie.addWord('yard')).to.equal(true)
			expect(trie.addWord('zoo')).to.equal(true)
			expect(trie.addWord('bar')).to.equal(true)

			// empty string
			expect(trie.addWord('')).to.equal(false)

			// total word count
			expect(trie.wordCount).to.equal(6)
		});
	});

	describe('Check if the word is in the trie', function(){
		it('Testing exists method. Checks each letter to see if the word is actually in the tree', function(){
			var trie = setUp();

			// should be in the trie
			expect(trie.exists('a')).to.equal(true)
			expect(trie.exists('ab')).to.equal(true)
			expect(trie.exists('abs')).to.equal(true)
			expect(trie.exists('yard')).to.equal(true)
			expect(trie.exists('zoo')).to.equal(true)
			expect(trie.exists('bar')).to.equal(true)

			// should NOT be in the trie
			expect(trie.exists('bare')).to.equal(false)
			expect(trie.exists('gorilla')).to.equal(false)
			expect(trie.exists('masterchief')).to.equal(false)
			expect(trie.exists('')).to.equal(false)
		});
	});

	describe('Number of Words In Trie', function(){
		it('Testing wordCount property. Should equal number of words inserted into Trie', function(){
			var trie = setUp();

			expect(trie.wordCount).to.equal(6)
		});
	});

	describe('Add And Check Every Word', function(){
		it('Add every word in English to the Trie and check if it exists', function(){
			createWordsArr(function(words){
				var trie = addWords(words);

				expect(checkWordsExist(words, trie)).to.equal(true)
				expect(checkWordsExist(['asdhsdfkjsdhf'], trie)).to.equal(false)
				expect(trie.wordCount).to.equal(words.length)
			})
		});
	});

})

function setUp(){
	var trie = new pt.Trie();

	trie.addWord('a')
	trie.addWord('ab')
	trie.addWord('abs')
	trie.addWord('yard')
	trie.addWord('zoo')
	trie.addWord('bar')

	return trie
}

// add all words in array to the Trie
function addWords(words){
	var trie = new Trie();
	for(var i in words){
		trie.addWord(words[i])
	}

	return trie
}

// check if all words in an array exist in the Trie
function checkWordsExist(trie, words){
	for(var i in words){
		var wordFound = trie.exists(words[i]);
		if(!wordFound){
			return false
		}
	}

	return true
}

// Create an array of words from dictionary file
function createWordsArr(cb){
	var arr = [];
	var rl = readline.createInterface({
		input: fs.createReadStream('../words.txt')
	});

	rl.on('line', (line) => {
		if(line.length > 0){
			arr.push(line)
		}
	})

	rl.on('close', () => {
		return cb(arr)
	})
}