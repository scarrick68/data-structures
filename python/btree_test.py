# This file is run using py.test
from btree import Btree
from btree import Node

# gotta find a way to test insert
def setUp():
    bst = Btree(20)
    bst.insert(10)
    bst.insert(30)
    bst.insert(15)
    bst.insert(25)
    return bst

def set_up_perfect_tree():
    bst = Btree(50)
    bst.insert(50)
    bst.insert(25)
    bst.insert(12)
    bst.insert(6)
    bst.insert(18)
    bst.insert(40)
    bst.insert(30)
    bst.insert(45)
    bst.insert(75)
    bst.insert(60)
    bst.insert(55)
    bst.insert(70)
    bst.insert(85)
    bst.insert(80)
    bst.insert(90)
    return bst

def test_tree_init():
    bst = Btree(10)
    assert bst.root.value == 10

def test_insert():
    bst = Btree(20)
    bst.insert(10)
    bst.insert(5)
    bst.insert(15)
    bst.insert(30)
    bst.insert(25)
    bst.insert(35)
    assert bst.root.left.value == 10 and bst.root.left.left.value == 5 and bst.root.left.right.value == 15 and bst.root.right.value == 30 and bst.root.right.left.value == 25 and bst.root.right.right.value == 35

def test_tree_length():
    bst = setUp()
    assert bst.length == 5

def test_inOrder_traversal():
    bst = setUp()
    assert bst.inOrder() == [10, 15, 20, 25, 30]

def test_preOrder_traversal():
    bst = setUp()
    assert bst.preOrder() == [20, 10, 15, 30, 25]

def test_postOrder_traversal():
    bst = setUp()
    assert bst.postOrder() == [15, 10, 25, 30, 20]

def test_node_removal():
    bst = set_up_perfect_tree()
    removed = bst.remove(6)
    assert removed == 6