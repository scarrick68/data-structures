# Notes and thoughts:
# 1. what to do when removing root node?
# 2. what to do if no value supplied to __init__
# 3. make some of these methods internal to the class.
# 	 maybe use wrapper functions to init values instead
# 	 of using kwargs like I am.

class Node:
	'Node in a btree'
	def __init__(self, value):
		self.value = value
		self.left = None
		self.right = None

class Btree:
	'binary search tree'
	def __init__(self, value):
		self.root = Node(value)
		self.length = 1

	# passing
	def insert(self, value, **kwargs):
		# assign root var
		root = self.assignRoot(kwargs)

		# insert node or recurse down until node can be inserted
		if value < root.value and root.left == None:
			root.left = Node(value)
			self.length = self.length + 1
			return
		elif value > root.value and root.right == None:
			root.right = Node(value)
			self.length = self.length + 1
			return
		elif value < root.value and root.left != None:
			self.insert(value, root=root.left)
		elif value > root.value and root.right != None:
			self.insert(value, root=root.right)
		else:
			print('something weird happened. aborting.')
			return

	#  Can't remove root node with this function. Can I implement this without a parent pointer?
	# test failing. probably need parent pointer.
	def remove(self, value, **kwargs):
		print('removing stuff')
		# assign root var
		root = self.assignRoot(kwargs)
		print(value, root.left.value)
		if value > root.value and root.right != None:
			self.remove(value, root=root.right)
		elif value < root.value and root.left != None:
			self.remove(value, root=root.left)
		if value == root.left.value:
			print('helloooooo')
			removed = root.left.value
			self.rearrangeSubtree(value, root)
			return removed
		if value == root.right.value:
			removed = root.right.value
			self.rearrangeSubtree(value, root)
			return removed

	# Little crowded with the comments here, but they make the case for each statement more obvious
	# root is the parent of the node being removed in this case
	# worried in case of a large tree and some removals near the top, tree will become very deep, but
	# only concerns for many nodes and specific removal pattern without periodic balancing.
	# also, just use an avl-tree or red black tree if you want. this implementation is for fun.
	def rearrangeSubtree(self, value, root):
		# node being removed is less than current node and has no children
		if value < root.value and root.left.left == None and root.left.right == None:
			root.left = None
		# node being removed is greater than current node and as no children
		elif value > root.value and root.right.left == None and root.right.right == None:
			root.right = None
		# node being removed is less and has no left children, but does have right children
		elif value < root.value and root.left.left == None and root.left.right != None:
			root.left = root.left.right
		# node being removed is less and has no right children, but does have left children
		elif value < root.value and root.left.right == None and root.left.left != None:
			root.left = root.left.left
		# node being removed is greater and has no left children, but does have right children
		elif value > root.value and root.right.left == None and root.right.right != None:
			root.right = root.right.right
		# node being removed is greater and has no right children, but does have left children
		elif value > root.value and root.right.right == None and root.right.left != None:
			root.right = root.right.left
		# node that has both left AND right children
		# Find the bottom of the left subtree of the right child and make the left subtree the left child of that
		elif value < root.value and root.left.left != None and root.left.right != None:
			# Doing this recursively seems more confusing, considering all the conditionals here
			bottom = root.left.right
			while root.left != None:
				bottom = root.left
			bottom.left = root.left.left
		# Find the bottom of the right subtree of the right child and make the left subtree the left child of that
		elif value > root.value and root.left.left != None and root.left.right != None:
			bottom = root.right.right
			while root.left != None:
				bottom = root.left
			bottom.left = root.right.left

	def exists(self):
		return

	def delete(self):
		return

	# passing
	def inOrder(self, **kwargs):
		root = self.assignRoot(kwargs)
		arr = self.assignArr(kwargs)
		# recurse down or 'visit' node
		if root.left != None:
			self.inOrder(arr=arr, root=root.left)
		arr.append(root.value)
		if root.right != None:
			self.inOrder(arr=arr, root=root.right)
		if len(arr) == self.length:
			return arr

	# passing
	def preOrder(self, **kwargs):
		# assign root var
		root = self.assignRoot(kwargs)
		arr = self.assignArr(kwargs)

		# recurse down or 'visit' node
		arr.append(root.value)
		if root.left != None:
			self.preOrder(arr=arr, root=root.left)
		if root.right != None:
			self.preOrder(arr=arr, root=root.right)
		if len(arr) == self.length:
			return arr

	# passing
	def postOrder(self, **kwargs):
		# assign root var
		root = self.assignRoot(kwargs)
		arr = self.assignArr(kwargs)

		# recurse down or 'visit' node
		if root.left != None:
			self.postOrder(arr=arr, root=root.left)
		if root.right != None:
			self.postOrder(arr=arr, root=root.right)
		arr.append(root.value)
		if len(arr) == self.length:
			return arr

	# I have these because idk how to handle optional args as
	# easily as I can in JS. Could wrap all functions, init
	# required fields and pass them on.
	def assignRoot(self, dict):
		# assign root var
		if 'root' not in dict:
			root = self.root
		if 'root' in dict:
			root = dict['root']
		return root

	def assignArr(self, dict):
		if 'arr' not in dict:
			arr = []
		if 'arr' in dict:
			arr = dict['arr']
		return arr

# bst = Btree(20)
# bst.insert(25)
# bst.insert(10)
# bst.insert(30)
# bst.insert(15)
# bst.remove(10)
# print('in order traversal')
# bst.inOrder()
# print('pre order traversal')
# bst.preOrder()
# print('post order traversal')
# bst.postOrder()