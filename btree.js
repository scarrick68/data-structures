'use strict'

function Btree(value){
	this.root = new Node(value) || new Node(0);
	this.length = 1;
}

function Node(value){
	this.value = value;
	this.leftChild = null;
	this.rightChild = null;
}

// find if a value is already in the tree
Btree.prototype.find = function(value, currentNode){
	// recursive base case. there's no such element in the tree.
	if(currentNode === null){
		return false;
	}
	// set initial node to start searching the tree
	if(currentNode === undefined){
		currentNode = this.root;
	}
	if(currentNode.value === value){
		return true;
	}
	else if(currentNode.value > value){
		this.find(value, currentNode.leftChild);
	}
	else if(currentNode.value < value){
		this.find(value, currentNode.rightChild)
	}
}

// find if a value is already in the tree
// Btree.prototype.find = function(value){
// 	var currentNode = this.root
// 	while(currentNode && (currentNode.leftChild || currentNode.rightChild)){
// 		if(value === currentNode.value){
// 			return value
// 		}
// 		else if(value > currentNode.value){
// 			currentNode = currentNode.rightChild
// 		}
// 		else if(value < currentNode.value){
// 			currentNode = currentNode.leftChild
// 		}
// 	}
// 	return false
// }

/*
	- Find leaf to attach new node
*/

// Insert element into the tree. Duplicates are not allowed.
Btree.prototype.insert = function(value, currentNode){
	// set initial node to start searching the tree
	if(currentNode === undefined){
		currentNode = this.root;
	}
	// Reached bottom of the tree. Insert element.
	if(value > currentNode.value && currentNode.rightChild === null){
		var node = new Node(value);
		currentNode.rightChild = node;
		this.length++;
		return value;
	}
	else if(value < currentNode.value && currentNode.leftChild === null){
		var node = new Node(value);
		currentNode.leftChild = node;
		this.length++;
		return value;	
	}

	if(currentNode.value === value){
		return false;
	}
	else if(currentNode.value > value){
		this.insert(value, currentNode.leftChild);
	}
	else if(currentNode.value < value){
	
		this.insert(value, currentNode.rightChild)
	}	
}

// meant to be called like tree.preorder and return an array of tree elements in preorder arrangement
// come back and think about why return this.preorder doesn't work
// come back and think about why checking for length at the end works and in the middle doesn't
Btree.prototype.preorder = function(arr, currentNode){
	if(currentNode === undefined){
		currentNode = this.root
		var arr = []
	}
	if(currentNode === null) return;
	arr.push(currentNode.value)
	this.preorder(arr, currentNode.leftChild);
	this.preorder(arr, currentNode.rightChild);
	if(arr.length === this.length)return arr;
};

Btree.prototype.inorder = function(arr, currentNode){
	if(currentNode === undefined){
		currentNode = this.root;
		var arr = [];
	}
	if(currentNode === null) return;
	this.inorder(arr, currentNode.leftChild);
	arr.push(currentNode.value);
	this.inorder(arr, currentNode.rightChild);
	if(arr.length === this.length) return arr;
}

Btree.prototype.postorder = function(arr, currentNode){
	if(currentNode === undefined){
		currentNode = this.root;
		var arr = [];
	}
	if(currentNode === null) return;
	this.postorder(arr, currentNode.leftChild)
	this.postorder(arr, currentNode.rightChild)
	arr.push(currentNode.value)
	if(arr.length === this.length) return arr;
}

Btree.prototype.balance = function(){
	// gives us an ordered array that we can use to rebuild the tree
}	

// return false if item cannot be found.
Btree.prototype.delete = function(value, currentNode){
	if(currentNode === undefined){
		currentNode = this.root;
	}
	
}

var tree = new Btree(1000);
tree.insert(100);
tree.insert(150);
tree.insert(1500);
tree.insert(2000);
tree.insert(1750);
tree.insert(450);
tree.insert(600);
tree.insert(375);

// for(let i = 0; i < 1000; i++){
// 	let value = Math.random() * 2000;
// 	if(!tree.find(value)){
// 		tree.insert(value);
// 	}
// }

console.log(tree.preorder());
console.log(tree.inorder());
console.log(tree.postorder());