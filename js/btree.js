/**
*   This is a binary tree that I'm writing for fun. It's just a good way to practice
*   recursion. I haven't made hard decisions on the API or properties. I think it's
*   pretty standard, but I reserve the right to make changes.
**/

// Properties of bst
// 1.   Creates Binary Search Tree
// 1a.  treeFromArr creates regular binary tree. Passing sorted array results in binary tree.
// 2.   Supports duplicates. N duplicate values require O(1) additional space.
// 3.   Insert
// 4.   Remove
// 5.   In-order traversal
// 6.   Pre-order traversal
// 7.   Post-order traversal
// 8.   Balance (this is not an AVL tree)

(function(){

    // Tree Node constructor
    function Node(val, parent, depth){
        this.val = val;
        this.left = null;
        this.right = null;
        this.parent = parent;
        this.depth = depth;
        this.count = 1;
    }

    // Tree constructor. I like the tree wrapper to track certain properties for O(1) access.
    function Btree(val){
        if(val){
            this.root = new Node(val);
            this.length = 1;
            this.root.depth = 1;
            this.max_depth = 1;
        }
        else{
            this.root = null;
            this.length = 0;
            this.max_depth = 0;
        }
    }

    // insert nodes into tree
    Btree.prototype.insert = function(val, currentNode, depth){
        currentNode = currentNode || this.root;
        depth = depth || 1;
        if(this.root === null){
            this.root = new Node(val, null, 1);
            this.length = 1;
            this.max_depth = 1;
            return
        }
        if(val === currentNode.val){
            currentNode.count++;
            return
        }
        if(val < currentNode.val && currentNode.left === null){
            currentNode.left = new Node(val, currentNode, depth+1);
            this.length++;
            if(currentNode.left.depth > this.max_depth){
                this.max_depth++;
            }
            return
        }
        else if(val > currentNode.val && currentNode.right === null){
            currentNode.right = new Node(val, currentNode, depth+1);
            this.length++;
            if(currentNode.right.depth > this.max_depth){
                this.max_depth++;
            }
            return
        }
        else if(val < currentNode.val && currentNode.left !== null){
            depth = depth+1;
            this.insert(val, currentNode.left, depth)
        }
        else if(val > currentNode.val && currentNode.right !== null){
            depth = depth+1;
            this.insert(val, currentNode.right, depth)
        }
        else{
            console.log('something weird happened')
        }
    };

    // find and return Node, return false if not found
    Btree.prototype.search = function(val, currentNode){
        currentNode = currentNode || this.root;
        if(val < currentNode.val && currentNode.left !== null){
            var found = this.search(val, currentNode.left)
        }
        else if(val > currentNode.val && currentNode.right !== null){
            var found = this.search(val, currentNode.right)
        }
        if(val === currentNode.val){
            var found = currentNode;
        }
        return found || false;
    };

    // remove Node and return node.val
    Btree.prototype.remove = function(val, currentNode){
        currentNode = currentNode || this.root;
        // currentNode has left and right children
        if(val === currentNode.val && currentNode.left && currentNode.right){
            findPrevious()
        }
        // currentNode is left child
        else if(val === currentNode.val && val < currentNode.parent.val){
            if(currentNode.left === null){
                var removedNode = currentNode
                currentNode.parent.left = currentNode.right;
                return currentNode
            }
            else if(currentNode.right === null){
                var removedNode = currentNode
                currentNode.parent.left = currentNode.left;
                return currentNode
            }
        }
        // currentNode is right child
        else if(val === currentNode.val && val > currentNode.parent.val){
            if(currentNode.left === null){
                var removedNode = currentNode
                currentNode.parent.right = currentNode.right;
                return currentNode
            }
            else if(currentNode.right === null){
                var removedNode = currentNode
                currentNode.parent.right = currentNode.left;
                return currentNode
            }
        }
    };

    // find previous value in tree. recurse to far right side of left subtree.
    function findPrevious(val, currentNode){

    }

    // in-order traversal, returns array
    Btree.prototype.inOrder = function(currentNode, arr){
        currentNode = currentNode || this.root;
        arr = arr || [];
        if(currentNode.left !== null){
            this.inOrder(currentNode.left, arr)
        }
        arr.push(currentNode.val);
        if(currentNode.right !== null){
            this.inOrder(currentNode.right, arr)
        }
        if(arr.length === this.length){
            return arr
        }
    };

    // pre-order traversal, returns array
    Btree.prototype.preOrder = function(currentNode, arr){
        currentNode = currentNode || this.root;
        arr = arr || [];
        arr.push(currentNode.val);
        if(currentNode.left !== null){
            this.preOrder(currentNode.left, arr)
        }
        if(currentNode.right !== null){
            this.preOrder(currentNode.right, arr)
        }
        if(arr.length === this.length){
            return arr
        }
    };

    // post-order traversal, returns array
    Btree.prototype.postOrder = function(currentNode, arr){
        currentNode = currentNode || this.root;
        arr = arr || [];
        if(currentNode.left !== null){
            this.postOrder(currentNode.left, arr)
        }
        if(currentNode.right !== null){
            this.postOrder(currentNode.right, arr)
        }
        arr.push(currentNode.val);
        if(arr.length === this.length){
            return arr
        }
    };

    Btree.prototype.bfs = function(){
        if(this.root === null){
            return []
        }
        var root = this.root;
        var queue = []              // using js array like q because it has flexible props
        queue.push(this.root)
        var results = []
        results.push(this.root.val)

        while(queue.length > 0){
            var r = queue.shift()
            // this counts as visiting for now
            if(r.left !== null){
                queue.push(r.left)
                results.push(r.left.val)
            }
            if(r.right !== null){
                queue.push(r.right)
                results.push(r.right.val)
            }
        }
        console.log(results)
        return results
    }

    // flatten tree and reconstruct balanced one
    // red-black and avl trees are more efficient alts
    // need to figure this out with new tree properties
    Btree.prototype.balance = function(currentNode){
        var sorted = this.inOrder(currentNode, []);
        var newTree = new Btree();
        newTree.fromArr(sorted, 0, sorted.length-1);
        this.root = newTree.root;
        this.max_depth = newTree.max_depth;
    };

    // wrapper function for creating tree from array
    // captures tree, returns Btree instance
    // calculating the length and depth here instead of as nodes are added
    // seems optimistic.
    function treeFromArr(arr){
        var bst = new Btree();
        bst.fromArr(arr, 0, arr.length-1);
        return bst
    }

    // create Btree from array
    // should probably figure out how to set max_depth in here instead of mathematically
    // in treeFromArr
    Btree.prototype.fromArr = function(arr, start, end, depth){
        depth = depth || 1;
        if(start > end){
            return null
        }
        var mid = Math.floor((start+end)/2);
        var node = new Node(arr[mid], null, depth);
        this.length = this.length+1;
        if(this.length === 1){
            this.root = node;
        }
        if(node.depth > this.max_depth){
            this.max_depth++;
        }
        node.left = this.fromArr(arr, start, mid-1, depth+1);
        if(node.left !== null){
            node.left.parent = node;
        }
        node.right = this.fromArr(arr, mid+1, end, depth+1);
        if(node.right !== null){
            node.right.parent = node;
        }
        return node
    };

    // assumes all nodes hold a number in val. can build check in later.
    Btree.prototype.sum = function(currentNode){
        currentNode = currentNode || this.root;
        if(currentNode.left === null && currentNode.right === null){
            return currentNode.val
        }
        return this.sum(currentNode.left) + currentNode.val + this.sum(currentNode.right)
    }

    module.exports = {
        Btree: Btree,
        treeFromArr: treeFromArr
    };
})();