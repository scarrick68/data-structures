(function(){

	function Trie(){
		this.root = new Node('root');
		this.wordCount = 0;
		this.totalNodes = 1;
	}

	function Node(letter){
		this.isWord = false;
		this.letter = letter || null;
		this.next = {};
	}

	// Add a word to the Trie
	// Trie cannot handle empty string rn
	// return true so I have an idicator of success. Possibly
	// useful in an app, but rn gives me more confidence in testing.
	Trie.prototype.addWord = function(word, currentNode){
		// get first letter of the word
		var letter = word.substring(0, 1)
		word = word.substring(1)
		currentNode = currentNode || this.root

		// create node for this letter if it doesn't exist
		// and if it's really a letter (no empty string allowed)
		if(!currentNode.next[letter] && letter){
			currentNode.next[letter] = new Node(letter);
			this.totalNodes++
		}
		// handle empty string.
		else if(!letter){
			return false
		}

		if(word.length === 0){
			currentNode.next[letter].isWord = true;
			this.wordCount++
			return true
		}

		return this.addWord(word, currentNode.next[letter])
	};

	// Check existence of a word in the Trie. else false.
	Trie.prototype.exists = function(word, currentNode){
		var letter = word.substring(0, 1)
		word = word.substring(1)
		currentNode = currentNode || this.root

		// word is in the Trie
		if(word.length === 0 && currentNode.next.hasOwnProperty(letter) && currentNode.next[letter].isWord){
			return true;
		}
		else if(currentNode.next.hasOwnProperty(letter)){
			var found = this.exists(word, currentNode.next[letter])
		}
		else{
			return false
		}

		return found
	};

	module.exports = {
		Trie: Trie
	};
})()