/**
*   This is mostly to get familiar with the chai and mocha testing tools. It
*   also provides tests for my binary tree, but the point is not that they
*   cover every single case.
**/

var Btree = require('../btree');
var expect = require('chai').expect;

// Need to make decisions about what happens with null root and make sure my
// tree functions account for it.

// TO TEST:
//      1. depth of each node (somewhat tested)
//      2. count for duplicates (somewhat tested)
//      3. regular removal
//      4. removal for duplicates
//      5. tree from array (somewhat tested)
//      6. balancing a tree (somewhat tested)

describe('Binary Search Tree Implementation', function(){
    describe('Number of Nodes', function(){
        it('Returns the number of nodes in the tree after only insertions', function(){
            var singleNode = singleNodeTree();
            var multiNode = setUp();

            expect(singleNode.length).to.equal(1);
            expect(multiNode.length).to.equal(7)
        });
    });

    describe('In-Order Traversal', function(){
        it('Returns an array containing tree contents in ascending order', function(){
            var singleNode = singleNodeTree();
            var multiNode = setUp();
            var singleNodeInOrder = singleNode.inOrder();
            var multiNodeInOrder = multiNode.inOrder();

            expect(singleNodeInOrder).to.deep.equal([5]);
            expect(multiNodeInOrder).to.deep.equal([12, 25, 30, 50, 60, 75, 80])
        })
    });

    describe('Pre-Order Traversal', function(){
        it('Returns an array containing tree contents from preorder traversal', function(){
            var singleNode = singleNodeTree();
            var multiNode = setUp();
            var singleNodePreOrder = singleNode.preOrder();
            var multiNodePreOrder = multiNode.preOrder();

            expect(singleNodePreOrder).to.deep.equal([5]);
            expect(multiNodePreOrder).to.deep.equal([50, 25, 12, 30, 75, 60, 80])
        });
    });

    describe('Post-Order Traversal', function(){
        it('Returns an array containing tree contents from postorder traversal', function(){
            var singleNode = singleNodeTree();
            var multiNode = setUp();
            var singleNodePostOrder = singleNode.postOrder();
            var multiNodePostOrder = multiNode.postOrder();

            expect(singleNodePostOrder).to.deep.equal([5]);
            expect(multiNodePostOrder).to.deep.equal([12, 30, 25, 60, 80, 75, 50])
        });
    });

    describe('Maximum Depth of Tree', function(){
        it('Testing Maximum Depth of Tree', function(){
            var singleNode = singleNodeTree();
            var multiNode = setUp();
            var arr = [12, 25, 30, 50, 60, 75, 80];
            var unbalanced = new Btree.Btree(arr[0]);
            for(var i = 1; i < arr.length; i++){
                    unbalanced.insert(arr[i]);
            }

            expect(singleNode.max_depth).to.equal(1);
            expect(multiNode.max_depth).to.equal(3);
            expect(unbalanced.max_depth).to.equal(7);

            unbalanced.balance();

            expect(unbalanced.max_depth).to.equal(3);
        });
    });

    describe('Depth of Nodes', function(){
        it('Testing Depth of Individual Nodes', function(){
            var singleNode = singleNodeTree();
            var multiNode = setUp();

            expect(singleNode.root.depth).to.equal(1);
            expect(multiNode.root.left.depth).to.equal(2);
            expect(multiNode.root.left.right.depth).to.equal(3);
            expect(multiNode.root.right.depth).to.equal(2);
            expect(multiNode.root.right.left.depth).to.equal(3);
        });
    });

    describe('Tree From Array', function(){
        it('Create a binary tree from an array', function(){
            var tree = Btree.treeFromArr([1,2,3,4,5,6,7]);

            expect(tree.root.val).to.equal(4);
            expect(tree.max_depth).to.equal(3);
        });
    });

    describe('Duplicate Node values', function(){
        it('Inserting multiple instances of the same value', function(){
            var bst = setUp();
            bst.insert(50);      // duplicate value at root
            bst.insert(30);      // duplicate value somewhere else
            bst.insert(75);
            bst.insert(60);
            bst.insert(75);
            bst.insert(80);

            expect(bst.root.count).to.equal(2);
            expect(bst.root.left.right.count).to.equal(2);
            expect(bst.root.right.left.count).to.equal(2);
            expect(bst.root.right.count).to.equal(3);
            expect(bst.root.right.right.count).to.equal(2);
        });
    });

    describe('Search For Value', function(){
        it('Searches the tree to see whether or not a given value exists', function(){
            var bst = setUp();
            var node1 = bst.search(50);
            var node2 = bst.search(25);
            var node3 = bst.search(75);
            var node4 = bst.search(30);
            var node5 = bst.search(12);
            var node6 = bst.search(60);
            var node7 = bst.search(80);

            // node1
            expect(node1.val).to.equal(50);
            expect(node1.count).to.equal(1);
            expect(node1.depth).to.equal(1);

            // node2
            expect(node2.val).to.equal(25);
            expect(node2.count).to.equal(1);
            expect(node2.depth).to.equal(2);

            // node3
            expect(node3.val).to.equal(75);
            expect(node3.count).to.equal(1);
            expect(node3.depth).to.equal(2);

            // node4
            expect(node4.val).to.equal(30);
            expect(node4.count).to.equal(1);
            expect(node4.depth).to.equal(3);

            // node5
            expect(node5.val).to.equal(12);
            expect(node5.count).to.equal(1);
            expect(node5.depth).to.equal(3);

            // node6
            expect(node6.val).to.equal(60);
            expect(node6.count).to.equal(1);
            expect(node6.depth).to.equal(3);

            // node7
            expect(node7.val).to.equal(80);
            expect(node7.count).to.equal(1);
            expect(node7.depth).to.equal(3);
        });
    });

    describe('Breadth First Search', function(){
        it('return array with tree contents in breadth first order', function(){
            var bst = setUp();
            var bfsOrder = bst.bfs()
            var sum = bst.sum()
            console.log('sum',sum)
            expect(bfsOrder).to.deep.equal([50, 25, 75, 12, 30, 60, 80])
        });
    });

    //describe('Remove Single Instance Nodes', function(){
    //    it('Remove Nodes that do not have any duplicates', function(){
    //        var bst = setUp();
    //        bst.
    //
    //        expect(bst.root.count).to.equal(2);
    //        expect(bst.root.left.right.count).to.equal(2);
    //        expect(bst.root.right.left.count).to.equal(2);
    //        expect(bst.root.right.count).to.equal(3);
    //        expect(bst.root.right.right.count).to.equal(2);
    //    });
    //});
});

function singleNodeTree(){
    var bst = new Btree.Btree(5);
    return bst
}

function setUp(){
    var bst = new Btree.Btree(50);
    bst.insert(25);
    bst.insert(75);
    bst.insert(60);
    bst.insert(30);
    bst.insert(12);
    bst.insert(80);
    return bst
}